package com.task;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class DayTest {
    private Day day;

    @Before
    public void setUp() {
        LineAvg line1 = new LineAvg("user2", "www.ololo.com", 50);
        LineAvg line2 = new LineAvg("user3", "www.azaza.com", 40);
        LineAvg line3 = new LineAvg("user4", "www.nlo.com", 60);
        List<LineAvg> lines = new ArrayList<>(3);
        lines.add(line1);
        lines.add(line2);
        lines.add(line3);
        day = new Day(new Date(), lines);

    }

    @Test
    public void addLine_addUser1_expectedRightOrder() {
        day.addLine(day.getDate().getTime() / 1000, "user1", "www.voditel", 30);
        LineAvg line1 = new LineAvg("user2", "www.ololo.com", 50);
        LineAvg line2 = new LineAvg("user3", "www.azaza.com", 40);
        LineAvg line3 = new LineAvg("user4", "www.nlo.com", 60);
        LineAvg line4 = new LineAvg("user1", "www.voditel", 30);
        List<LineAvg> lines = new ArrayList<>(3);
        lines.add(line4);
        lines.add(line1);
        lines.add(line2);
        lines.add(line3);
        assertEquals(lines, day.getLines());
        assertEquals("user1", day.getLines().get(0).getUserId());
    }

    @Test
    public void addLine_addUser2TheSame_expectedRightTime() {
        day.addLine(day.getDate().getTime() / 1000, "user2", "www.ololo.com", 30);
        assertEquals(40, day.getLines().get(0).getTime());
    }

    @Test
    public void addLine_addWrongDay_expectedNotToAdd() {
        List<LineAvg> lines = day.getLines();
        day.addLine((day.getDate().getTime() / 1000) + 24 * 60 * 60, "user2", "www.ololo.com", 30);
        assertEquals(lines, day.getLines());
    }
}
