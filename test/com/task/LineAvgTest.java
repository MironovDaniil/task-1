package com.task;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class LineAvgTest {

    private LineAvg lineAvg;

    @Before
    public void setUp() {
        lineAvg = new LineAvg("Gavr", "www.ololo.com", 50);
    }

    @Test
    public void addSession_addTheSame_shouldSucceed_avgTime60() {
        boolean success = lineAvg.addSession("Gavr", "www.ololo.com", 70);
        assertTrue(success);
        assertEquals(60, lineAvg.getTime());
    }

    @Test
    public void addSession_addDifferent_shouldFail() {
        boolean success = lineAvg.addSession("Gavr", "www.olol.com", 70);
        assertFalse(success);
        assertNotEquals(60, lineAvg.getTime());
    }

    @Test
    public void addSession_addTheSameTwice_shouldSucceed_avgTime70() {
        lineAvg.addSession("Gavr", "www.ololo.com", 70);
        boolean success = lineAvg.addSession("Gavr", "www.ololo.com", 90);
        assertTrue(success);
        assertEquals(70, lineAvg.getTime());
    }

}
