package com.task;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import static org.mockito.Mockito.mock;


import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class FileHandlerTest {

    @Rule
    public TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void handle_checkFileHandling() throws IOException {
        File file = folder.newFile("myfile.txt");
        PrintWriter out = new PrintWriter(file.getAbsoluteFile());
        out.println("1455812018,user2,http://ru.wikipedia.org,100 \n" +
                "1455812019,user10,http://hh.ru,30\n" +
                "1455812968,user3,http://google.com,60\n" +
                "1455812411,user10,http://hh.ru,90\n" +
                "1455812684,user3,http://vk.com,50");
        out.close();
        FileHandler handler = new FileHandler(file);
        List<Day> actual = handler.handleCSV();
        for (Day d : actual
        ) {
            for (LineAvg line:d.getLines()
            ) {
                System.out.println(line.getUserId()+","+line.getUrl()+","+line.getTime());
            }
        }
        assertEquals(60,actual.get(0).getLines().get(3).getTime());
    }

}
