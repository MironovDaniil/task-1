package com.task;



import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.*;

class FileHandler {

    private final SimpleDateFormat FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    private final File file;

    FileHandler(File file) {
        this.file = file;
    }

    private boolean validateXMLByXSD(File xml, File xsd) throws Exception, IOException {
        try {
            SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
                    .newSchema(xsd)
                    .newValidator()
                    .validate(new StreamSource(xml));
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void handleCSV(File outFile) throws FileNotFoundException {

        List<Day> result = new ArrayList<>();
        Scanner in = new Scanner(file);
        while (in.hasNextLine()) {
            String line = in.nextLine();
            String[] words = line.split(",");
            if (!words[0].matches("[-+]?\\d+")) {
                continue;
            }
            long timeStamp = Long.parseLong(words[0]);
            String userId = words[1];
            String url = words[2];
            int timeSpent = Integer.parseInt(words[3].replaceAll("[^0-9]", ""));
            Date time1 = new java.util.Date((long) timeStamp * 1000);
            Date time2 = new java.util.Date((long) (timeStamp + timeSpent) * 1000);
            if (!FORMAT.format(time1).equals(FORMAT.format(time2))) {
                int k = 0;
                for (int i = 1; i < timeSpent; i++) {
                    Date timei = new java.util.Date((long) (timeStamp + i) * 1000);
                    if (FORMAT.format(timei).equals(FORMAT.format(time2))) {
                        k = i;
                        break;
                    }
                }
                long timeStamp2 = timeStamp + k;
                int timeSpent1 = k;
                int timeSpent2 = timeSpent - k;
                boolean wasAdded = false;
                for (Day d : result
                ) {
                    wasAdded = d.addLine(timeStamp, userId, url, timeSpent1);
                }
                if (!wasAdded) {
                    Day day1 = new Day(time1);
                    day1.addLine(timeStamp, userId, url, timeSpent1);
                    result.add(day1);
                } else {
                    wasAdded = false;
                }
                for (Day d : result
                ) {
                    wasAdded = d.addLine(timeStamp2, userId, url, timeSpent2);
                }
                if (!wasAdded) {
                    Day day2 = new Day(new java.util.Date((long) timeStamp2 * 1000));
                    day2.addLine(timeStamp2, userId, url, timeSpent2);
                    result.add(day2);
                }


            } else {
                boolean wasAdded = false;
                for (Day d : result
                ) {
                    wasAdded = d.addLine(timeStamp, userId, url, timeSpent);
                }
                if (!wasAdded) {
                    Day day1 = new Day(time1);
                    day1.addLine(timeStamp, userId, url, timeSpent);
                    result.add(day1);
                }
            }
        }
        Collections.sort(result);
        PrintWriter out = new PrintWriter(outFile.getAbsoluteFile());
        out.println("UserId" + "," + "Url" + "," + "AverageTimeSpent");
        for (Day d : result
        ) {
            out.println(FORMAT.format(d.getDate()));
            for (LineAvg line : d.getLines()
            ) {
                out.println(line.getUserId() + "," + line.getUrl() + "," + line.getTime());
            }
        }
        out.close();
    }

    private void handleXML(File outFile) throws JAXBException {
        List<Day> result = new ArrayList<>();

        JAXBContext context = JAXBContext
                .newInstance(InputBody.class);
        Unmarshaller um = context.createUnmarshaller();

        InputBody body = (InputBody) um.unmarshal(file);

        for (InputLine line : body.getLines()
        ) {
            long timeStamp = line.getTimeStamp();
            String userId = line.getUserId();
            String url = line.getUrl();
            int timeSpent = line.getTimeSpent();
            Date time1 = new java.util.Date((long) timeStamp * 1000);
            Date time2 = new java.util.Date((long) (timeStamp + timeSpent) * 1000);
            if (!FORMAT.format(time1).equals(FORMAT.format(time2))) {
                int k = 0;
                for (int i = 1; i < timeSpent; i++) {
                    Date timei = new java.util.Date((long) (timeStamp + i) * 1000);
                    if (FORMAT.format(timei).equals(FORMAT.format(time2))) {
                        k = i;
                        break;
                    }
                }
                long timeStamp2 = timeStamp + k;
                int timeSpent1 = k;
                int timeSpent2 = timeSpent - k;
                boolean wasAdded = false;
                for (Day d : result
                ) {
                    wasAdded = d.addLine(timeStamp, userId, url, timeSpent1);
                }
                if (!wasAdded) {
                    Day day1 = new Day(time1);
                    day1.addLine(timeStamp, userId, url, timeSpent1);
                    result.add(day1);
                } else {
                    wasAdded = false;
                }
                for (Day d : result
                ) {
                    wasAdded = d.addLine(timeStamp2, userId, url, timeSpent2);
                }
                if (!wasAdded) {
                    Day day2 = new Day(new java.util.Date((long) timeStamp2 * 1000));
                    day2.addLine(timeStamp2, userId, url, timeSpent2);
                    result.add(day2);
                }


            } else {
                boolean wasAdded = false;
                for (Day d : result
                ) {
                    wasAdded = d.addLine(timeStamp, userId, url, timeSpent);
                }
                if (!wasAdded) {
                    Day day1 = new Day(time1);
                    day1.addLine(timeStamp, userId, url, timeSpent);
                    result.add(day1);
                }
            }
        }
        Collections.sort(result);
        DayContainer days = new DayContainer();
        days.setDays(result);
        context = JAXBContext
                .newInstance(DayContainer.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        m.marshal(days, outFile);
    }

    void fullHandle() {

        File outFile = new File(file.getParentFile().getAbsolutePath() + "\\" + "avg_" + file.getName());

        if (file.getName().toUpperCase().endsWith(".CSV")) {
            try {
                handleCSV(outFile);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
        if (file.getName().toUpperCase().endsWith(".XML")) {
            try {
                if (!validateXMLByXSD(file, new File("C:\\Users\\Varamadon\\Documents\\Proga\\inputFiles\\inputSchema.xsd"))) {
                    System.out.println("Got wrong xml file, doing nothing!");
                    return;
                }
                handleXML(outFile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
