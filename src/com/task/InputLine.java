package com.task;

import javax.xml.bind.annotation.XmlElement;

public class InputLine {
    private long timeStamp;
    private String userId;
    private String url;
    private int timeSpent;

    public InputLine(long timeStamp, String userId, String url, int timeSpent) {
        this.timeStamp = timeStamp;
        this.userId = userId;
        this.url = url;
        this.timeSpent = timeSpent;
    }

    public InputLine() {
        this(0, "", "", 0);
    }

    @XmlElement(name = "timeStamp")
    long getTimeStamp() {
        return timeStamp;
    }

    @XmlElement(name = "userId")
    String getUserId() {
        return userId;
    }

    @XmlElement(name = "url")
    String getUrl() {
        return url;
    }

    @XmlElement(name = "timeSpent")
    int getTimeSpent() {
        return timeSpent;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setTimeSpent(int timeSpent) {
        this.timeSpent = timeSpent;
    }
}
