package com.task;

import javax.xml.bind.annotation.XmlElement;
import java.util.Objects;

public class LineAvg implements Comparable<LineAvg> {
    private final String userId;
    private final String url;
    private int time;
    private int sessionsCount;

    LineAvg () {
        this("","",0);
    }

    LineAvg(String userId, String url, int time) {
        this.userId = userId;
        this.url = url;
        this.time = time;
        sessionsCount = 1;
    }

    boolean addSession(String userId, String url, int time) {
        if (userId.equals(this.userId) && url.equals(this.url)) {
            sessionsCount++;
            this.time = ((this.time * (sessionsCount - 1)) + time) / sessionsCount;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public int compareTo(LineAvg lineAvg) {
        int id1 = Integer.parseInt(this.userId.replaceAll("[^0-9]", ""));
        int id2 = Integer.parseInt(lineAvg.userId.replaceAll("[^0-9]", ""));
        return id1-id2;
    }

    @XmlElement(name = "userId")
    String getUserId() {
        return userId;
    }

    @XmlElement (name = "url")
    String getUrl() {
        return url;
    }

    @XmlElement (name = "time")
    int getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LineAvg)) return false;
        LineAvg lineAvg = (LineAvg) o;
        return getTime() == lineAvg.getTime() &&
                Objects.equals(getUserId(), lineAvg.getUserId()) &&
                Objects.equals(getUrl(), lineAvg.getUrl());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getUrl(), getTime());
    }
}
