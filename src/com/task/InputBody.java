package com.task;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "lines")
public class InputBody {

    private List<InputLine> lines;

    public InputBody(List<InputLine> lines) {
        this.lines = lines;
    }

    public InputBody() {
        this(new ArrayList<>());
    }

    @XmlElement(name = "line")
    List<InputLine> getLines() {
        return lines;
    }

    public void setLines(List<InputLine> lines) {
        this.lines = lines;
    }
}
