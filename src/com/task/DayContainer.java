package com.task;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

@XmlRootElement(name = "days")
public class DayContainer {
    private List<Day> days;

    public DayContainer(List<Day> days) {
        this.days = days;
    }

    public DayContainer() {
        this(new ArrayList<>());
    }

    public void setDays(List<Day> days) {
        this.days = days;
    }

    @XmlElement(name = "day")
    public List<Day> getDays() {
        return days;
    }
}
