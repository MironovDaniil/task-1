package com.task;

import javax.xml.bind.annotation.XmlElement;
import java.text.SimpleDateFormat;
import java.util.*;

public class Day implements Comparable<Day> {
    private final Date date;
    private List<LineAvg> lines;
    private final SimpleDateFormat FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    Day() {
        this(new Date());
    }

    Day(Date date) {
        this.date = date;
        lines = new ArrayList<>();
    }

    Day(Date date, List<LineAvg> lines) {
        this.date = date;
        this.lines = lines;
    }

    private void sortDay() {
        Collections.sort(lines);
    }

    boolean addLine(long time, String userId, String url, int timeSpent) {
        Date time1 = new java.util.Date((long) time * 1000);
        Date time2 = new java.util.Date((long) (time + timeSpent) * 1000);
        if (!FORMAT.format(time1).equals(FORMAT.format(time2))) {
            Date time3 = new java.util.Date((long) (time + timeSpent - 1) * 1000);
            if (!FORMAT.format(time1).equals(FORMAT.format(time3))) {
                System.out.println("Added line is not in one day!"+Thread.currentThread().getName());
                return false;
            }
        }
        if (!FORMAT.format(time1).equals(FORMAT.format(this.getDate()))) {
            return false;
        }
        for (LineAvg line : lines
        ) {
            if (line.addSession(userId, url, timeSpent)) {
                return true;
            }
        }
        lines.add(new LineAvg(userId, url, timeSpent));
        sortDay();
        return true;
    }

    boolean addLine(InputLine inputLine) {
        return addLine(
                inputLine.getTimeStamp(),
                inputLine.getUserId(),
                inputLine.getUrl(),
                inputLine.getTimeSpent());
    }


    Date getDate() {
        return date;
    }

    @XmlElement(name = "date")
    String getFormatedDate() {
        return FORMAT.format(date);
    }

    @XmlElement(name = "line")
    List<LineAvg> getLines() {
        return lines;
    }

    @Override
    public int compareTo(Day day) {
        return this.date.compareTo(day.getDate());
    }
}
