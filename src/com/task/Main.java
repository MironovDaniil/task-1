package com.task;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.SchemaOutputResolver;
import javax.xml.transform.Result;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Main {

    private static final String SoursePath = "C:\\Users\\Varamadon\\Documents\\Proga\\inputFiles";    //Input directory

    public static void main(String[] args) throws Exception, IOException {

        System.out.println("Running!");
        File directory;
        if (args.length==0) {
            directory = new File(SoursePath);
        } else {
            directory = new File(args[0]);
        }
        if (!directory.isDirectory()) {
            System.out.println("Got not a directory! Program stop");
            return;
        }
        ExecutorService executor = Executors.newFixedThreadPool(10);
        int iterationNumber = 0;

        JAXBContext jaxbContext = JAXBContext.newInstance(InputBody.class);
        SchemaOutputResolver sor = new SchemaOutputResolver() {
            @Override
            public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
                File file = new File(directory.getAbsolutePath()+"\\inputSchema.xsd");
                StreamResult result = new StreamResult(file);
                result.setSystemId(file.toURI().toURL().toString());
                return result;
            }
        };
        jaxbContext.generateSchema(sor);

        jaxbContext = JAXBContext.newInstance(DayContainer.class);
        sor = new SchemaOutputResolver() {
            @Override
            public Result createOutput(String namespaceUri, String suggestedFileName) throws IOException {
                File file = new File(directory.getAbsolutePath()+"\\outputSchema.xsd");
                StreamResult result = new StreamResult(file);
                result.setSystemId(file.toURI().toURL().toString());
                return result;
            }
        };
        jaxbContext.generateSchema(sor);

        while (true) {
            File[] folderEntries = directory.listFiles();
            iterationNumber++;
            if (folderEntries.length == 0) {
                continue;
            }

            for (File entry : folderEntries) {
                if ((!entry.getName().toUpperCase().endsWith(".CSV"))
                        && (!entry.getName().toUpperCase().endsWith(".XML"))) {
                    continue;
                }
                if (entry.getName().startsWith("avg_")) {
                    continue;
                }
                File file1 = new File(entry.getParentFile().getAbsolutePath()
                        + "\\" + "avg_" + entry.getName());
                boolean wasCreated = false;
                if (file1.exists()) {
                    continue;
                } else {
                    try {
                        wasCreated = file1.createNewFile();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                if (!wasCreated) {
                    System.out.println("Something is wrong, can't create file!" + iterationNumber);
                    continue;
                }
                executor.submit(() -> {
                    FileHandler handler = new FileHandler(entry);
                    handler.fullHandle();
                });
            }

        }


    }

}
